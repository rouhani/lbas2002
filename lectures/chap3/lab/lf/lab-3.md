# Lab-3

### Læringsutbytte

* Sammenligne strenger
* Velge egnet løkkekonstruksjon (for eller while) etter behov

### Læringsaktiviteter

* [Sammenligning av strenger](sammenligning_av_strenger.ipynb)
* [Introduksjon til løkker](intro_til_lokker.ipynb)
* [Gjettelek](gjett_tallet.ipynb)
